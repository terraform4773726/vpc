variable "vpc_cidr" {
	default ="10.0.0.0/27"
}	

variable "subnet1_cidr" {
	default ="10.0.0.0/28"
}

variable "subnet2_cidr" {
	default="10.0.0.16/28"
}

variable "amiid" {
	default="ami-053b0d53c279acc90"
}

variable "type" {
	default="t2.micro"
}

variable "pemfile" {
	default = "AWSkeypair"
}
