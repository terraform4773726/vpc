provider "aws" {
	region="us-east-1"
}

resource "aws_vpc" "my_vpc" {
	cidr_block=var.vpc_cidr
	enable_dns_hostnames= true

tags={
	Name="my_vpc"
}
}

data "aws_availability_zones" "all" {}

resource "aws_subnet" "public_subnet" {
	vpc_id=aws_vpc.my_vpc.id
	cidr_block=var.subnet1_cidr
	availability_zone = data.aws_availability_zones.all.names[0]

tags={
	Name="public subnet"
}
}

resource "aws_internet_gateway" "vpc_igw" {
	vpc_id=aws_vpc.my_vpc.id
	
tags={
	Name="My_igw"
}
}

resource "aws_route_table" "publicRT" {
	vpc_id =aws_vpc.my_vpc.id

	route {
	cidr_block="0.0.0.0/0"
	gateway_id=aws_internet_gateway.vpc_igw.id
}
tags={
	Name="RT-public"
}
}

resource "aws_route_table_association" "my_vpc_ass-public" {
	subnet_id=aws_subnet.public_subnet.id
	route_table_id=aws_route_table.publicRT.id
}

resource "aws_security_group" "instances" {
	name="my_example_instance"
	vpc_id=aws_vpc.my_vpc.id

	egress {
		from_port=0
		to_port=0
		protocol= "-1"
		cidr_blocks=["0.0.0.0/0"]
		}
	
	ingress {
		from_port=22
		to_port=22
		protocol="tcp"
		cidr_blocks=["0.0.0.0/0"]
		}
	
	ingress {
		from_port=8080
		to_port=8080
		protocol="tcp"
		cidr_blocks=["0.0.0.0/0"]
}
}

resource "aws_instance" "server" {
	ami=var.amiid
	instance_type=var.type
	key_name=var.pemfile
	vpc_security_group_ids=[aws_security_group.instances.id]
	availability_zone=data.aws_availability_zones.all.names[0]
	subnet_id=aws_subnet.public_subnet.id
	associate_public_ip_address=true
	user_data = <<-EOF
                #!/bin/bash
                echo "Welcome to Manju's world" > index.html
                nohup busybox httpd -f -p 8080 &
                EOF
tags={
	Name="webserver"
}
}

resource "aws_subnet" "private_subnet" {
	vpc_id = aws_vpc.my_vpc.id
	cidr_block=var.subnet2_cidr
	availability_zone=data.aws_availability_zones.all.names[1]

tags={
	Name="private subnet"
}
}

resource "aws_route_table" "privateRT" {
	vpc_id = aws_vpc.my_vpc.id

route {
	cidr_block="0.0.0.0/0"
	network_interface_id=aws_instance.server.primary_network_interface_id
	}
tags={
	Name="RT private"
}
}

resource "aws_route_table_association" "my_vpc_ass_private" {
	subnet_id=aws_subnet.private_subnet.id
	route_table_id=aws_route_table.privateRT.id
}


resource "aws_security_group" "db" {
	name="my_example_db"
	vpc_id=aws_vpc.my_vpc.id
	
	egress {
	from_port=0
	to_port =0
	protocol ="-1"
	cidr_blocks=["0.0.0.0/0"]
	}
	
	ingress {
	from_port=3306
	to_port=3306
	protocol ="tcp"
 	security_groups=[aws_security_group.instances.id]
	}
}


resource "aws_instance" "dbserver" {
	ami=var.amiid
	instance_type= var.type
	key_name=var.pemfile
	vpc_security_group_ids=[aws_security_group.db.id]
	availability_zone=data.aws_availability_zones.all.names[1]
	subnet_id = aws_subnet.private_subnet.id
	associate_public_ip_address=true

tags={
Name="dbserver"
}
}
